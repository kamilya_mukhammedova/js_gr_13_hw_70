export class ApplicationForm {
  constructor(
    public id: string,
    public name: string,
    public surname: string,
    public patronymic: string,
    public phoneNumber: string,
    public jobOrStudyPlace: string,
    public skills: { skill: string, levelOfSkill: string }[],
    public tShirtVariant: string,
    public size: string,
    public comment: string
  ) {}
}
