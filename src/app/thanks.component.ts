import { Component } from '@angular/core';

@Component({
  selector: 'app-thanks',
  template: `
    <h2 class="mt-5">Your registration is accepted, thank you!</h2>
  `,
  styles: [`
  h2 { color: red }
  `]
})
export class ThanksComponent {
}
